﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Concurrent;

namespace DomainDrivenDesign.TestDomain.Core
{
    [TestClass]
    public class TestObservable
    {
        [TestMethod]
        public void IntObserve()
        {
          var observable =new Observable<string>();

            observable.Subscribe(o => Console.WriteLine(o), oe => Console.WriteLine(oe),
                () => Console.WriteLine("Complelte"));

            observable.Publish("Hi, here is the first message");
            observable.Publish("Hi, here is the seconds message");

            observable.Complete();
        }

     
    }

    public class Observable<T> : IObservable<T>
    {
        class Observe : IObserver<T>
        {
            private readonly Action<T> _onNext;
            private readonly Action<Exception> _onError;
            private readonly Action _onComplete;

            public Observe(Action<T> onNext, Action<Exception> onError, Action onComplete)
            {
                _onNext = onNext;
                _onError = onError;
                _onComplete = onComplete;
            }

            public void OnNext(T value)
            {
                _onNext(value);
            }

            public void OnError(Exception error)
            {
                _onError(error);
            }

            public void OnCompleted()
            {
                _onComplete();
            }
        }

        ConcurrentDictionary<Type, IObserver<T>> _map = new ConcurrentDictionary<Type, IObserver<T>>();

        public IObservable<T> Subscribe(Action<T> onNext, Action<Exception> onError, Action onComplete)
        {
            var type = typeof(T);

            var obs = new Observe(onNext, onError, onComplete);

            if (!_map.ContainsKey(type))
            {
                _map[type] = obs;
            }

            return null;
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            Subscribe(observer.OnNext, observer.OnError, observer.OnCompleted);
            return null;
        }

        public void Publish(T obj)
        {
            var type = typeof(T);
            IObserver<T> found;
            if (_map.TryGetValue(type, out found))
            {

                try
                {
                    found.OnNext(obj);
                }
                catch (Exception e)
                {
                    found.OnError(e);
                }
            }
        }

        public void Complete()
        {
            var type = typeof(T);
            IObserver<T> found;
            if (_map.TryGetValue(type, out found))
            {

                try
                {
                    found.OnCompleted();
                }
                catch (Exception e)
                {
                    found.OnError(e);
                }
            }
        }

        public void Unsubscribe()
        {
            var type = typeof(T);
            IObserver<T> old;
            _map.TryRemove(type, out old);
        }
    }
}
