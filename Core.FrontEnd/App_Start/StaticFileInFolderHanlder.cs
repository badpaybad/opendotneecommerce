﻿using System.IO;
using System.Web;
using System.Web.Routing;

namespace Core.FrontEnd
{
    public class Angular6RoutingHandler : IRouteHandler {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new Angular6HttpHanlder();
        }
    }

    public class Angular6HttpHanlder : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var requestPhysicalPath = context.Request.PhysicalPath;
            if (File.Exists(requestPhysicalPath))
            {
                WriteFile(context,requestPhysicalPath);
            }
            else
            {
                WriteFile(context,context.Server.MapPath("~/home-cook/index.html"));
            }
            context.Response.Flush();
            context.Response.End();
        }

        public bool IsReusable { get { return true; } }


        void WriteFile(HttpContext context, string pathFile)
        {
            //context.Response.ContentType = MimeMapping.GetMimeMapping(pathFile);
            context.Response.WriteFile(pathFile);
        }
    }

}