﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Core.FrontEnd;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(Core.FrontEnd.SignalrStartup))]
namespace Core.FrontEnd
{
    public class SignalrStartup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            //app.Use(async (context, next) => {
            //    await next();

            //    if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
            //    {
            //        context.Request.Path = new PathString("/home-cook"); // Put your Angular root page here 
            //        await next();
            //    }
            //});

            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();

           
        }
    }
}