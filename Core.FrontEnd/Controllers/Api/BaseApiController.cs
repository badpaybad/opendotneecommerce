using System.Web.Http;
using System.Web.Http.Cors;
using DomainDrivenDesign.Core.Utils;

namespace Core.FrontEnd.Controllers.Api
{
   
    public class BaseApiController : ApiController
    {
        public string SiteDomainUrl
        {
            get { return HttpHelper.GetRootWeb(); }
        }
    }
}