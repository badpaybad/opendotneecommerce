﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using DomainDrivenDesign.Core.Implements;
using DomainDrivenDesign.Core.Implements.Models;
using DomainDrivenDesign.CoreCms.Ef;
using DomainDrivenDesign.CoreEcommerce.Ef;
using DomainDrivenDesign.CoreEcommerce.Services;

namespace Core.FrontEnd.Controllers.Api
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    [System.Web.Http.Route("api/homepage")]
    public class HomePageController : BaseApiController
    {
        [Route("api/homepage/getcombos")]
        public IEnumerable<Combo> GetCombos(Guid categoryId, int? quantity = 6)
        {
            quantity = quantity ?? 6;

            long total;
            List<ContentLanguage> contentLangs;
            var products = ProductSearchServices.Search(string.Empty, null, new List<Guid>() { categoryId }, true, 0, quantity.Value,
                 out contentLangs, out total);

            var domainUrl = SiteDomainUrl;

            foreach (var p in products)
            {
                yield return new Combo()
                {
                    Id = p.Id,
                    Price = p.Price,
                    ProductCode = p.ProductCode,
                    ImageUrl = $"{domainUrl}{contentLangs.GetValue(p.Id, "UrlImage")}",
                    UrlDetail = $"{domainUrl}/product/{contentLangs.GetValue(p.Id, "SeoUrlFriendly")}",
                };
            }
        }

        [Route("api/homepage/getmenus")]
        public IEnumerable<Menu> GetMenus(Guid categoryId, int? quantity = 6)
        {
            List<ContentLanguage> contentLangs;
            List<Category> categories;
            using (var db = new CoreEcommerceDbContext())
            {
                categories = db.Categories.Where(i => i.ParentId == categoryId).ToList();
                var ids = categories.Select(i => i.Id).ToList();
                contentLangs = db.ContentLanguages.Where(i => ids.Contains(i.Id)).ToList();
            }

            var domainUrl = SiteDomainUrl;
            foreach (var c in categories)
            {
                yield return new Menu
                {
                    Id = c.Id,
                    Title = contentLangs.GetValue(c.Id, "Title"),
                    ImageUrl = $"{domainUrl}{contentLangs.GetValue(c.Id, "UrlImage")}",
                    UrlDetail = $"{domainUrl}/product/{contentLangs.GetValue(c.Id, "SeoUrlFriendly")}",
                };
            }
        }

        [Route("api/homepage/getdrinks")]
        public IEnumerable<Drink> GetDrinks(Guid categoryId, int? quantity = 6)
        {
            quantity = quantity ?? 6;

            long total;
            List<ContentLanguage> contentLangs;
            var products = ProductSearchServices.Search(string.Empty, null, new List<Guid>() { categoryId }, true, 0, quantity.Value,
                out contentLangs, out total);

            var domainUrl = SiteDomainUrl;
           
            foreach (var p in products)
            {
                yield return new Drink()
                {
                    Id = p.Id,
                    Price = p.Price,
                    ProductCode = p.ProductCode,
                    ImageUrl = $"{domainUrl}{contentLangs.GetValue(p.Id, "UrlImage")}",
                    Title = contentLangs.GetValue(p.Id, "Title"),
                    UrlDetail = $"{domainUrl}/product/{contentLangs.GetValue(p.Id, "SeoUrlFriendly")}",
                };
            }
        }
        [Route("api/homepage/getproductdetail")]
        public ProductDetail GetProductDetail(Guid id)
        {
            ProductDetail p;
            List<ContentLanguage> contentLangs;
            using (var db = new DomainDrivenDesign.CoreEcommerce.Ef.CoreEcommerceDbContext())
            {
                p = db.Products.Where(i => i.Id == id).Select(i => new ProductDetail()
                {
                    Id=i.Id,
                    Price=i.Price,
                    Gram = i.Gram,
                    Calorie = i.Calorie,
                    ProductCode = i.ProductCode,
            }).FirstOrDefault();

                contentLangs = db.ContentLanguages.Where(i => i.Id == id).ToList();

            }

            var domainUrl = SiteDomainUrl;

            p.Title = contentLangs.GetValue(id, "Title");
            p.Description = contentLangs.GetValue(id, "Description");
            p.ImageUrl = $"{domainUrl}{contentLangs.GetValue(p.Id, "UrlImage")}";
            p.UrlDetail = $"{domainUrl}/product/{contentLangs.GetValue(p.Id, "SeoUrlFriendly")}";
            return p;
        }

        [Route("api/homepage/getproducts")]
        public IEnumerable<ProductDetail> GetProducts(Guid categoryId, int? quantity = 6)
        {
            quantity = quantity ?? 6;

            long total;
            List<ContentLanguage> contentLangs;
            var products = ProductSearchServices.Search(string.Empty, null, new List<Guid>() { categoryId }, true, 0, quantity.Value,
                out contentLangs, out total);

            var domainUrl = SiteDomainUrl;
         
            foreach (var p in products)
            {
                yield return new ProductDetail()
                {
                    Id = p.Id,
                    Price = p.Price,
                    Gram = p.Gram,
                    Calorie = p.Calorie,
                    ProductCode = p.ProductCode,
                    Title = contentLangs.GetValue(p.Id, "Title"),
                    Description = contentLangs.GetValue(p.Id, "Description"),
                    ImageUrl = $"{domainUrl}{contentLangs.GetValue(p.Id, "UrlImage")}",
                    UrlDetail = $"{domainUrl}/product/{contentLangs.GetValue(p.Id, "SeoUrlFriendly")}",
                };
            }
        }
    }

    public class ProductDetail
    {
        public Guid Id;
        public string ImageUrl;
        public string UrlDetail;
        public long Price;
        public string Title;
        public string ProductCode;
        public string Description;
        public int Gram { get; set; }
        public int Calorie { get; set; }
    }

    public class Combo
    {
        public Guid Id;
        public string ImageUrl;
        public string UrlDetail;
        public long Price;
        public string ProductCode;
    }

    public class Drink
    {
        public Guid Id;
        public string ImageUrl;
        public string UrlDetail;
        public long Price;
        public string Title;
        public string ProductCode;
    }

    public class Menu
    {
        public Guid Id;
        public string Title;
        public string UrlDetail;
        public string ImageUrl;
    }
}