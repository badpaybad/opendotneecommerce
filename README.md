# README #

- .Net framework 4.6.1
- Entity framework 6.0.0
- Newtonjson 11.0.0.0
- System.Data.SqlClient 4.4.2
- MySql.Data 6.10.6.0
- MySql.Data.Entity.EF6 6.9.11.0
- Mysql, MsSql 2008 or later ( can use any kind of db, just add provider for Entity framework)


### What is this repository for? ###

* Support developer to quick develop cms, ecommerce or anything base on CQRS pattern
* Version 1.0.0
* Any question can ask on http://badpaybad.info or badpaybad@gmail.com

### How do I get set up? ###

* Open Ms visual studio 2017
* Clone git and rebuild solution to restore nuget package
* Change connection string in web.config
* I use IDatabaseInitializer so that db will auto init
* Set Core.FrontEnd to default project
* Press F5 to build and then try

### Contribution guidelines ###

* Hope this will help devloper to easy develop any thing
