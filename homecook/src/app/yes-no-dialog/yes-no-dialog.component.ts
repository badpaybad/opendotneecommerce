import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-yes-no-dialog',
  templateUrl: './yes-no-dialog.component.html',
  styleUrls: ['./yes-no-dialog.component.css']
})
export class YesNoDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<YesNoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
   
  }

  ngOnInit() {
    if (this.data.Title == null || this.data.Title == '') {
      this.data.Title = 'Confirm your action?';
    }
    if (this.data.Message == null || this.data.Message == '') {
      this.data.Message = 'Confirm your action?';
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick() {
    return true;
  }
  
}
