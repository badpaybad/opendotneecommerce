import { Component, Pipe, Directive, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';

import { Meta, Title } from '@angular/platform-browser';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatBottomSheet, MatBottomSheetRef, MatBottomSheetConfig } from '@angular/material/bottom-sheet';
import { ProductDetailBottomSheetComponent } from './product-detail-bottom-sheet/product-detail-bottom-sheet.component';

import { SystemNotificationService } from './system-notification.service';
import { YesNoDialogComponent } from './yes-no-dialog/yes-no-dialog.component';
import { AppConfigService } from './app-config.service';
import { NotificationMessage } from './view-model/NotificationMessage';
import { HomePageService } from './home-page.service';
import { MiniCart, ShoppingCartItem } from './view-model/ShoppingCart';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = '';
  @ViewChild("sidenav")
  sidenav: any;
  @ViewChild("sidenavRight")
  sidenavRight: any;
  miniCart: MiniCart = { Id: '', TotalQuantity: 0 };
  tinyMceData:any;
  showSlideUpdownMenuTop = 'display:none';
  keywordsToSearch = '';
  isHandset: boolean;

  public constructor(private breakpointObserver: BreakpointObserver,
    private bottomSheet: MatBottomSheet, private titleService: Title,
    private notifyService: SystemNotificationService,
    private homePageService: HomePageService,
    public appConfig: AppConfigService,
    public dialog: MatDialog,
    private router: Router) {
    }

  public ngOnInit() {
    this.setTitle('home-cook');

    this.breakpointObserver.observe(Breakpoints.Handset).subscribe((s: BreakpointState) => {
      this.isHandset = s.matches;
    });
    
    this.homePageService.addedToCart.subscribe((id) => {
      this.sidenavRight.open();
    });

    this.homePageService.miniCartFilled.subscribe((cart) => {
      this.miniCart = cart;
    });

    const self = this;

    window.onscroll = function() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        self.showSlideUpdownMenuTop = '';
        document.getElementById("slideUpdownMenuTop").style.top = "0";
      } else {
        self.showSlideUpdownMenuTop = 'display:none';
        document.getElementById("slideUpdownMenuTop").style.top = "-50px";
      }

      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("btnGotoTop").style.display = "block";
      } else {
        document.getElementById("btnGotoTop").style.display = "none";
      }
    };
   

    document.addEventListener('DOMContentLoaded', () => {   
      this.video1PlayPause();
    });

  }
  gotoTopFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  navigateTo(routeLink: string) {
    this.router.navigateByUrl(routeLink);
  }

  public setTitle(newTitle: string) {
    this.title = newTitle;

    this.titleService.setTitle(newTitle);
  }

  public onMenuClicked(mnRouteLink: string) {
    this.sidenav.close();
  }
  
  openProductDetail(productId: string) {
    var ref=this.bottomSheet.open(ProductDetailBottomSheetComponent, { data: { productId: productId } });
  }
  
  clearCart() {
    var dialogRef = this.dialog.open(YesNoDialogComponent,{
        width: '250px',
        data: { Title: 'Clear shopping cart?', Message: 'Your all shopping cart items will be remove' }
      });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.homePageService.clearCart();
        this.miniCart = { Id: '', TotalQuantity: 0 };
      }
    });
  }

  doSearch(keywords) {
    if (keywords == '') return;

    console.log(keywords);
  }

  video1PlayPause() { 
    let myVideo  =<HTMLVideoElement> document.getElementById("video1"); 
    if (myVideo.paused) 
      myVideo.play(); 
    else 
      myVideo.pause(); 
  } 
}
