import { TestBed, inject } from '@angular/core/testing';

import { SystemNotificationService } from './system-notification.service';

describe('SystemNotificationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SystemNotificationService]
    });
  });

  it('should be created', inject([SystemNotificationService], (service: SystemNotificationService) => {
    expect(service).toBeTruthy();
  }));
});
