import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleMapPickAddressComponent } from './google-map-pick-address.component';

describe('GoogleMapPickAddressComponent', () => {
  let component: GoogleMapPickAddressComponent;
  let fixture: ComponentFixture<GoogleMapPickAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleMapPickAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleMapPickAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
