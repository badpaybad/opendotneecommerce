import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { GoogleMapAddressPicked } from '../view-model/ShoppingCart';

declare const google: any, $: any;


@Component({
  selector: 'app-google-map-pick-address',
  templateUrl: './google-map-pick-address.component.html',
  styleUrls: ['./google-map-pick-address.component.css']
})

export class GoogleMapPickAddressComponent implements OnInit {
  @Output() addressPicked = new EventEmitter<GoogleMapAddressPicked>();
  @Output() checkShippingCost = new EventEmitter<GoogleMapAddressPicked>();
  $txtLat: any;
  $txtLng: any;
  $txtSearchDom: any;

  constructor() { }

  ngOnInit() {
    this.initGoogleMap();
  }

  pacInputTextChange() {

  }

  initGoogleMap() {
    var _this = this;
    var cancelAutoDetectLocation = false;

    _this.$txtLat = $('#pac-input-lat');
    _this.$txtLng = $('#pac-input-lng');

    let txtSearchDom = document.getElementById('pac-input');
    let mapDom = document.getElementById('map');
    _this.$txtSearchDom = $(txtSearchDom);

    _this.$txtSearchDom.blur(function() {
      _this.addressPicked.emit({
        Address: _this.$txtSearchDom.val(),
        Lat: _this.$txtLat.val(),
        Lng: _this.$txtLng.val()
      });
    });

    var map = new google.maps.Map(mapDom,
      {
        center: { lat: 21.0286234, lng: 105.8437302 },
        zoom: 13,
        mapTypeId: 'roadmap'
      });

    // Create the search box and link it to the UI element.
    var searchBox = new google.maps.places.SearchBox(txtSearchDom);

    var marker = new google.maps.Marker({
      position: { lat: 21.0286234, lng: 105.8437302 },
      map: map,
      title: 'Your are here'
    });

    marker.setMap(map);
    map.panTo(marker.position);
    _this.$txtLat.val(marker.position.lat);
    _this.$txtLng.val(marker.position.lng);

    _this.addressPicked.emit({
      Address: _this.$txtSearchDom.val(),
      Lat: _this.$txtLat.val(),
      Lng: _this.$txtLng.val()
    });

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (browserPosition) {
        if (cancelAutoDetectLocation) return;
        marker.setMap(null);
        marker = new google.maps.Marker({
          map: map,
          title: 'Your are here',
          position: { lat: browserPosition.coords.latitude, lng: browserPosition.coords.longitude }
        });
        marker.setMap(map);
        map.panTo(marker.position);
        _this.$txtLat.val(browserPosition.coords.latitude);
        _this.$txtLng.val(browserPosition.coords.longitude);

        _this.addressPicked.emit({
          Address: _this.$txtSearchDom.val(),
          Lat: _this.$txtLat.val(),
          Lng: _this.$txtLng.val()
        });
      });
    }

    map.addListener('click',
      function (e) {
        cancelAutoDetectLocation = true;
        marker.setMap(null);
        marker = new google.maps.Marker({
          map: map,
          title: 'Your are here',
          position: e.latLng
        });
        marker.setMap(map);
        map.panTo(marker.position);
        _this.$txtLat.val(marker.position.lat);
        _this.$txtLng.val(marker.position.lng);

        _this.addressPicked.emit({
          Address: _this.$txtSearchDom.val(),
          Lat: _this.$txtLat.val(),
          Lng: _this.$txtLng.val()
        });
      });

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed',
      function () {
        searchBox.setBounds(map.getBounds());
      });

    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed',
      function () {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
          return;
        }
        cancelAutoDetectLocation = true;
        // Clear out the old markers.
        marker.setMap(null);

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
          if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
          }

          // Create a marker for each place.
          marker = new google.maps.Marker({
            map: map,
            title: place.name,
            position: place.geometry.location
          });

          _this.$txtLat.val(marker.position.lat);
          _this.$txtLng.val(marker.position.lng);

          _this.addressPicked.emit({
            Address: _this.$txtSearchDom.val(),
            Lat: _this.$txtLat.val(),
            Lng: _this.$txtLng.val()
          });

          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
      });
  }

  doCheckShippingCost() {
    this.checkShippingCost.emit({
      Address: this.$txtSearchDom.val(),
      Lat: this.$txtLat.val(),
      Lng: this.$txtLng.val()
    });
  }
}
