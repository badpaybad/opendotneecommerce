import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDetailBottomSheetComponent } from './product-detail-bottom-sheet.component';

describe('ProductDetailBottomSheetComponent', () => {
  let component: ProductDetailBottomSheetComponent;
  let fixture: ComponentFixture<ProductDetailBottomSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDetailBottomSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDetailBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
