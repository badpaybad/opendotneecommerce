import { Component, OnInit, EventEmitter, Input, Inject, AfterContentInit, AfterViewInit, ChangeDetectorRef} from '@angular/core';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';

import { ComboHomePage, DrinkHomePage, MenuHomePage, ProductDetailHomePage } from '../view-model/ComboHomePage';
import { HomePageService } from '../home-page.service';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ImageSliderDialogComponent } from '../image-slider-dialog/image-slider-dialog.component';


@Component({
  selector: 'app-product-detail-bottom-sheet',
  templateUrl: './product-detail-bottom-sheet.component.html',
  styleUrls: ['./product-detail-bottom-sheet.component.css']
})
export class ProductDetailBottomSheetComponent implements OnInit, AfterContentInit, AfterViewInit {
  
  public product = new ProductDetailHomePage();

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
    private homePageService: HomePageService,
    private bottomSheetRef: MatBottomSheetRef<ProductDetailBottomSheetComponent>,
    public dialog: MatDialog, private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.homePageService.getProductDetail(this.data.productId).subscribe(p => {
      this.product = p;
      this.changeDetectorRef.markForCheck();
    });
  }
  ngAfterContentInit() {
   
  }
  ngAfterViewInit() {
   
  }

  close() {
    this.bottomSheetRef.dismiss();
  }

  addToCart() {
    this.homePageService.addToCart(this.product.Id, 1);
    this.bottomSheetRef.dismiss();
  }

  showImageSlider() {
    var urlImgs : string[]=[];
    urlImgs.push(this.product.ImageUrl);
    var d = this.dialog.open(ImageSliderDialogComponent, { width: '100%', data: {Title: this.product.Title ,urlImages:urlImgs}});
  }
}
