import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-image-slider-dialog',
  templateUrl: './image-slider-dialog.component.html',
  styleUrls: ['./image-slider-dialog.component.css']
})
export class ImageSliderDialogComponent implements OnInit {
  title='';
  urlImages:Array<string>=[];

  constructor(public dialogRef: MatDialogRef<ImageSliderDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    
  }

  ngOnInit() {
    this.title = this.data.Title;
    this.urlImages = this.data.urlImages;
    this.urlImages.push('./assets/imgs/icon.png');
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  getFirstUrlImage() {
    return this.urlImages.length > 0 ? this.urlImages[0] : '../../assets/imgs/icon.png';
  }
}
