export class MiniCart {
  Id: string;
  TotalQuantity: number;
}


export class ShoppingCartResponse {
  Data: any;
}

export class ShoppingCartItem {
  ProductId: string;
  Title: string;
  ProductCode: string;
  UrlImage: string;
  SeoUrlFriendly: string;
  UnitPrice: number;
  Quantity: number;
  TotalPrice: number;
  PromotionDescription: string;
  ProductPromotionId: string;
}


export class GoogleMapAddressPicked {
  Address: string;
  Lat: number;
  Lng: number;
}

export class FeShoppingCartCheckoutPage {
  Id: string;
  PaymentMethods: Array<FeIdAndDescription> = [];
  ShippingMethods: Array<FeIdAndDescription> = [];
  CartItemCount: number;
  OrderPromotion = new FeOrderPromotion();
  CartTotal: number;
  CartDiscount: number;
}
export class FeIdAndDescription {
  Id: string;
  Name: string;
  Description: string;
}
export class FeOrderPromotion {
  Id: string;
  AmountToDiscount: number;
  DiscountAmount: number;
  FreeShip: number;
  Actived: boolean;
  CreatedDate: Date;
  Description: string;
}
