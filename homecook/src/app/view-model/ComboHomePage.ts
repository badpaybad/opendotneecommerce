
export class ComboHomePage {
  ImageUrl:string;
  UrlDetail:string;
  Price:number;
  ProductCode:string;
  Id: string;
}


export class MenuHomePage {
  ImageUrl: string;
  UrlDetail: string;
  Title: string;
  Id: string;
}


export class DrinkHomePage {
  ImageUrl: string;
  UrlDetail: string;
  Price: number;
  ProductCode: string;
  Title:string;
  Id: string;
}



export class ProductDetailHomePage {
  ImageUrl: string;
  UrlDetail: string;
  Price: number;
  ProductCode: string;
  Title: string;
  Id: string;
  Description: string;
  Gram: number;
  Calorie: number;
}

