import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-menu-top',
  templateUrl: './menu-top.component.html',
  styleUrls: ['./menu-top.component.css']
})

export class MenuTopComponent implements OnInit {
  menu_items: Array<MenuItem> = [];
  isHandset: Observable<BreakpointState>;

  @Output() menuClicked = new EventEmitter<string>();

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private titleServices: Title
    , private breakpointObserver: BreakpointObserver) {

  }

  ngOnInit() {
    this.isHandset = this.breakpointObserver.observe(Breakpoints.Handset);

    this.router.config.forEach(mn => {
      //  console.log(mn);
      if (mn.data && mn.data.title && !mn.data.hide) {
        this.menu_items.push({
          path: '/' + mn.path,
          title: mn.data.title
        });
      }
    });

    this.router.events.subscribe(re => {
      if (re instanceof NavigationStart) {
        this.menu_items.forEach(mni => {
          if (re.url == mni.path) {
            this.titleServices.setTitle(mni.title);
            return;
          }
        });
      }

    });

  }

  navigateTo(routeLink: string) {
    this.router.navigateByUrl(routeLink);
    this.menuClicked.emit(routeLink);
  }

}

class MenuItem {
  title: string = '';
  path: string = '';
}
