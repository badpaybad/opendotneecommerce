import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealsMenuComponent } from './meals-menu.component';

describe('MealsMenuComponent', () => {
  let component: MealsMenuComponent;
  let fixture: ComponentFixture<MealsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealsMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
