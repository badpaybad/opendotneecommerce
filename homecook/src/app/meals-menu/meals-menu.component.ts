import { Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { MatBottomSheet, MatBottomSheetRef, MatBottomSheetConfig } from '@angular/material/bottom-sheet';

import { ComboHomePage, DrinkHomePage, MenuHomePage, ProductDetailHomePage } from '../view-model/ComboHomePage';
import { HomePageService } from '../home-page.service';

import { ProductDetailBottomSheetComponent } from '../product-detail-bottom-sheet/product-detail-bottom-sheet.component';
import { MenuDetailBottomSheetComponent } from '../menu-detail-bottom-sheet/menu-detail-bottom-sheet.component';

@Component({
  selector: 'app-meals-menu',
  templateUrl: './meals-menu.component.html',
  styleUrls: ['./meals-menu.component.css']
})
export class MealsMenuComponent implements OnInit {
  breakpoint = 3;
  isHandset: Observable<BreakpointState>;
  combos: Array<ComboHomePage>=[];
  drinks: Array<DrinkHomePage>=[];
  menus: Array<MenuHomePage> = [];

  @ViewChild("sidenavRight") sidenavRight: any;

  constructor(private bottomSheet: MatBottomSheet,
    private breakpointObserver: BreakpointObserver, private homePageService: HomePageService) {

  }

  ngOnInit() {

    this.isHandset = this.breakpointObserver.observe(Breakpoints.Handset);
    this.breakpoint = (window.innerWidth <= 700) ? 1 : 3;

    this.homePageService.getCombos().subscribe(combos => {
      
      this.combos = combos;
    });

    this.homePageService.getMenus().subscribe(menus => {
      this.menus = menus;
    });

    this.homePageService.getDrinks().subscribe(drinks => {
      this.drinks = drinks;
    });
  }
  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 700) ? 1 : 3;
  }

  openComboDetail(productId: string) {
    let pdbs = this.bottomSheet.open(ProductDetailBottomSheetComponent, { data: { productId: productId } });
   
  }


  openDrinkDetail(productId: string) {
    this.bottomSheet.open(ProductDetailBottomSheetComponent , { data: { productId: productId } });
  }

  openMenuDetail(categoryId: string, categoryTitle:string) {
    this.bottomSheet.open(MenuDetailBottomSheetComponent, { data: { categoryId: categoryId, categoryTitle: categoryTitle } });
  }

  addToCart(productId) {
    this.homePageService.addToCart(productId,1);
  }

}
