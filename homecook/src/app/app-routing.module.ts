import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from './home-page/home-page.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { MyDashboardComponent } from './my-dashboard/my-dashboard.component';
import { MyNavComponent } from './my-nav/my-nav.component';
import { MyTableComponent } from './my-table/my-table.component';
import { ShoppingcartCheckoutComponent } from './shoppingcart-checkout/shoppingcart-checkout.component';

const routes: Routes = [
  //{ path: '', redirectTo: 'home-page', pathMatch:'full'  },
   { path: 'home-page', component: HomePageComponent,data:{title:'Home'} },
  { path: 'shoppingcart-checkout', component: ShoppingcartCheckoutComponent, data: { title: 'Checkout' } },
  { path: '', component: HomePageComponent, data: { title: 'Home', hide: true } },
  { path: '**', redirectTo: 'home-page', pathMatch: 'full' }
  //{ path: 'about-us', component: AboutUsComponent ,data:{title:'About us'}},
  //{ path: 'my-nav', component: MyNavComponent ,data:{title:'My nav'}},
  //{ path: 'my-dashboard', component: MyDashboardComponent ,data:{title:'My dashboard'}},
  //{ path: 'my-table', component: MyTableComponent ,data:{title:'My table'}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
