import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpClientModule, HttpHeaders, HttpParams } from '@angular/common/http';
import { AppConfigService } from './app-config.service';
import { ComboHomePage, DrinkHomePage, MenuHomePage, ProductDetailHomePage } from './view-model/ComboHomePage';
import { MiniCart, ShoppingCartResponse, ShoppingCartItem, FeShoppingCartCheckoutPage } from './view-model/ShoppingCart';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class HomePageService {
  miniCartFilled = new EventEmitter<MiniCart>();
  fullCartItemsFilled = new EventEmitter<Array<ShoppingCartItem>>();
  addedToCart = new EventEmitter<string>();

  currentCartId = '';
  currentItems: Array<ShoppingCartItem> = [];

  _localDb = window.localStorage;

  constructor(private http: HttpClient, private appConfig: AppConfigService) {
    this.currentCartId = this._localDb.getItem('currentCartId');
    if (this.currentCartId == null) {
      this.currentCartId = '';
    }

    this.getMiniCart();
  }

  getCombos(): Observable<ComboHomePage[]> {
    let arr: Array<ComboHomePage> = [];
    arr.push({
      ImageUrl:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVECl0N5fazcY_MnTkZPZiuRIs9BlZy_yA5fjjUOMQ7q4w9ml54w&s",
      UrlDetail:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVECl0N5fazcY_MnTkZPZiuRIs9BlZy_yA5fjjUOMQ7q4w9ml54w&s",
      Price: 12000,
      ProductCode:"CO123",
      Id: "123"
    });
    return of(arr);
    //return this.http.get<ComboHomePage[]>(this.appConfig.apiUrl + '/homepage/GetCombos?categoryId=' + this.appConfig.mealsId, httpOptions);
    //let arr: Array<ComboHomePage> = [];
    //arr.push({ ImageUrl: 'img', UrlDetail: 'url', Price:0, ProductCode:'code' });
    //return of(arr);
  }

  getMenus(): Observable<MenuHomePage[]> {
    let arr: Array<MenuHomePage> = [];
    arr.push({
      ImageUrl:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuycNJ7XG5hKI0BZClQn-7BOmj6vh1hhV2cVuLxYy2FUKgvrPA&s",
      UrlDetail:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuycNJ7XG5hKI0BZClQn-7BOmj6vh1hhV2cVuLxYy2FUKgvrPA&s",      
      Title:"Combo breakfast",
      Id: "123"
    });
    return of(arr);
    // return this.http.get<MenuHomePage[]>(this.appConfig.apiUrl + '/homepage/GetMenus?categoryId=' + this.appConfig.menusId, httpOptions);
  }

  getDrinks(): Observable<DrinkHomePage[]> {
    let arr: Array<DrinkHomePage> = [];
    arr.push({
      ImageUrl:"https://www.graphicsfactory.com/clip-art/image_files/image/0/635710-1004food013.gif",
      UrlDetail:"https://www.graphicsfactory.com/clip-art/image_files/image/0/635710-1004food013.gif",      
      Price: 24000,
      Title:"Combo hot summer",
      ProductCode:"COHS123",
      Id: "123"
    });
    return of(arr);
    //return this.http.get<DrinkHomePage[]>(this.appConfig.apiUrl + '/homepage/GetDrinks?categoryId=' + this.appConfig.drinksId, httpOptions);
  }

  getProductDetail(id: string): Observable<ProductDetailHomePage> {
    let arr: ProductDetailHomePage = {
      ImageUrl: "https://cdn.imgbin.com/11/19/1/imgbin-food-wine-glass-dinner-cocktail-junk-food-MBPHEM6c1uhxLqEupZ1f8YtFw.jpg",
      UrlDetail: "https://cdn.imgbin.com/11/19/1/imgbin-food-wine-glass-dinner-cocktail-junk-food-MBPHEM6c1uhxLqEupZ1f8YtFw.jpg",
      Price: 123,
      ProductCode: "Co123",
      Title: "Title here",
      Id: "1234",
      Description: "Description here",
      Gram: 120,
      Calorie: 1000
    }   ;
  
    return of(arr);
   // return this.http.get<ProductDetailHomePage>(this.appConfig.apiUrl + '/homepage/GetProductDetail?id=' + id, httpOptions);
  }

  getProductsInMenu(id: string): Observable<Array<ProductDetailHomePage>> {
    let arr: Array<ProductDetailHomePage> = [];
    arr.push({
      ImageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6S0_ewWEaFpNywk7fNdDwxayTj6qGKfP6FEvsKYHeWPkX4vTMFA&s",
      UrlDetail: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6S0_ewWEaFpNywk7fNdDwxayTj6qGKfP6FEvsKYHeWPkX4vTMFA&s",
      Price: 123,
      ProductCode: "Co123",
      Title: "Title here",
      Id: "1234",
      Description: "Description here",
      Gram: 120,
      Calorie: 1000
    });
    return of(arr);
   // return this.http.get<Array<ProductDetailHomePage>>(this.appConfig.apiUrl + '/homepage/GetProducts?categoryId=' + id, httpOptions);
  }

  getMiniCart() {
   
    //return;
    // this.http.get<ShoppingCartResponse>(this.appConfig.shoppingCartUrl + '/ShoppingCart/GetMiniCart?id='
    //   + this.currentCartId, httpOptions).subscribe((cart) => {
    //     this.miniCartFilled.emit(cart.Data);
    //     if (this.currentCartId == '') {
    //       this.currentCartId = cart.Data.Id;
    //       this._localDb.setItem('currentCartId', this.currentCartId);
    //     }
    //   });
  }

  getCheckoutPageData(): Observable<ShoppingCartResponse> {
    let arr: ShoppingCartResponse = new ShoppingCartResponse();
    return of(arr);
    // return this.http.get<ShoppingCartResponse>(this.appConfig.shoppingCartUrl + '/ShoppingCart/GetCheckoutPageData?id='
    //   + this.currentCartId, httpOptions);
  }


  getCartItems() {
    let arr=[];
    return of(arr);
    // return this.http.post<any>(this.appConfig.shoppingCartUrl + '/ShoppingCart/ListResult', { id: this.currentCartId }, httpOptions)
    //   .subscribe((res) => {
    //     this.currentItems = res.rows;
    //     this.fullCartItemsFilled.emit(this.currentItems);

    //   });
  }


  addToCart(productId: string, quantity: number) {
    return;

    // this.http.get<ShoppingCartResponse>(this.appConfig.shoppingCartUrl + '/ShoppingCart/AddProductToShoppingCart' +
    //   '?productId=' + productId + '&quantity=' + quantity + '&id=' + this.currentCartId, httpOptions)
    //   .subscribe(() => {
    //     this.addedToCart.emit(productId);
    //     this.getMiniCart();
    //     this.getCartItems();
    //   });
  }

  removeFromCart(productId: string, quantity: number) {
    return;
    // this.http.get<ShoppingCartResponse>(this.appConfig.shoppingCartUrl + '/ShoppingCart/RemoveProductFromShoppingCart' +
    //   '?productId=' + productId + '&quantity=' + quantity + '&id=' + this.currentCartId, httpOptions)
    //   .subscribe(() => {
    //     this.addedToCart.emit(productId);
    //     this.getMiniCart();
    //     this.getCartItems();
    //   });
  }

  removeAllFromCart(productId: string) {
    return;
    // var temp = this.currentItems.filter(p => p.ProductId == productId);
    // if (temp.length > 0) {
    //   this.http.get<ShoppingCartResponse>(this.appConfig.shoppingCartUrl + '/ShoppingCart/RemoveProductFromShoppingCart' +
    //     '?productId=' + productId + '&quantity=' + temp[0].Quantity + '&id=' + this.currentCartId, httpOptions)
    //     .subscribe(() => {
    //       this.addedToCart.emit(productId);
    //       this.getMiniCart();
    //       this.getCartItems();
    //     });
    // }
  }

  clearCart() {
    this.currentCartId = '';
    this._localDb.setItem('currentCartId', '');
    this.currentItems = [];
    this.getMiniCart();
    this.getCartItems();
  }

  completeCheckoutAndDoPayment(paymentMethodId: any, shippingMethodId: any
    , receivingTime: Date, address: string, addressLatitude: number, addressLongitude: number
    , addressName: string, phone: string, email: string
    , voucherCode: string, message: string): Observable<ShoppingCartResponse> {
    const data = {
      id: this.currentCartId,
      paymentMethodId: paymentMethodId,shippingMethodId: shippingMethodId,
      receivingTime: receivingTime, address: address, addressLatitude: addressLatitude, addressLongitude: addressLongitude,
      addressName: addressName, phone: phone, email: email, voucherCode: voucherCode, message: message
    };
    return this.http.post<ShoppingCartResponse>(this.appConfig.shoppingCartUrl + '/ShoppingCart/CompleteAndPayOrder',
      data, httpOptions);
  }

}
