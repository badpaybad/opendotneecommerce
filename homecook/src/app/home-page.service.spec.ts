import { TestBed, inject } from '@angular/core/testing';

import { HomePageServicesService } from './home-page.service';

describe('HomePageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HomePageServicesService]
    });
  });

  it('should be created', inject([HomePageService], (service: HomePageServicesService) => {
    expect(service).toBeTruthy();
  }));
});
