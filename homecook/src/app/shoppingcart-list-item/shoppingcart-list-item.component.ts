import { Component, OnInit } from '@angular/core';
import { NotificationMessage } from '../view-model/NotificationMessage';
import { HomePageService } from '../home-page.service';
import { MiniCart, ShoppingCartItem } from '../view-model/ShoppingCart';
import { MatBottomSheet, MatBottomSheetRef, MatBottomSheetConfig } from '@angular/material/bottom-sheet';
import { ProductDetailBottomSheetComponent } from '../product-detail-bottom-sheet/product-detail-bottom-sheet.component';


@Component({
  selector: 'app-shoppingcart-list-item',
  templateUrl: './shoppingcart-list-item.component.html',
  styleUrls: ['./shoppingcart-list-item.component.css']
})
export class ShoppingcartListItemComponent implements OnInit {
  cartItems: Array<ShoppingCartItem> = [];

  shoppingCartColumns = ['UrlImage', 'ProductCode', 'TotalPrice', 'Id'];

  constructor(private bottomSheet: MatBottomSheet, private homePageService: HomePageService) { }

  ngOnInit() {
    this.homePageService.getCartItems();
    this.homePageService.fullCartItemsFilled.subscribe((cartItems) => {
      this.cartItems = cartItems;
    });
  }

  getSubTotalShoppingCart() {
    return this.cartItems.map(m => m.TotalPrice).reduce((a, b) => a + b, 0);
  }

  addOneProductToCart(productId: string) {
    this.homePageService.addToCart(productId, 1);
  }

  removeOneProductFromCart(productId: string) {
    this.homePageService.removeFromCart(productId, 1);
  }

  removeAllProductFromCart(productId: string) {
    this.homePageService.removeAllFromCart(productId);
  }
   
  openProductDetail(productId: string) {
    var ref = this.bottomSheet.open(ProductDetailBottomSheetComponent, { data: { productId: productId } });
  }  
}
