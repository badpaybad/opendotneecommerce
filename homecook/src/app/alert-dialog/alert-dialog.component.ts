import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.css']
})
export class AlertDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AlertDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

  }

  ngOnInit() {
    if (this.data.Title == null || this.data.Title == '') {
      this.data.Title = 'Confirm your action?';
    }
    if (this.data.Message == null || this.data.Message == '') {
      this.data.Message = 'Confirm your action?';
    }

    if (this.data.Icon == null || this.data.Icon == '') {
      this.data.Icon = 'warning';
    }
  }

  onOkClick(): void {
    this.dialogRef.close();
  }
}
