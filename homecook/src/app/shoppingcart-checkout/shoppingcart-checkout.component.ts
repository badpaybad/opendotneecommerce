import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';


import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

import { GoogleMapAddressPicked } from '../view-model/ShoppingCart';
import { HomePageService } from '../home-page.service';
import { MiniCart, ShoppingCartItem, FeShoppingCartCheckoutPage } from '../view-model/ShoppingCart';

export class KeyValue {
  key: string;
  value: string;
}

@Component({
  selector: 'app-shoppingcart-checkout',
  templateUrl: './shoppingcart-checkout.component.html',
  styleUrls: ['./shoppingcart-checkout.component.css']
})

export class ShoppingcartCheckoutComponent implements OnInit {
 
  checkoutPageData = new FeShoppingCartCheckoutPage();
  breakpoint = 2;
  isHandset: Observable<BreakpointState>;
  minDate = new Date();
  maxDate = new Date();
  hours: Array<KeyValue> = [];
  minutes: Array<KeyValue> = [];

  paymentMethod: string;
  shippingMethod: string;
  selectedDate: Date;
  selectedHour = '--';
  selectedMinute = '--';
  voucherCode: '';
  fullName = "";
  phone = "";
  email = "";
  note = "";
  addressPicked = new GoogleMapAddressPicked();

  constructor(private homeServices: HomePageService, public dialog: MatDialog,
    private breakpointObserver: BreakpointObserver, private changeDetectorRef: ChangeDetectorRef) {
    const now = new Date();
    this.minDate = new Date();
    now.setDate(now.getDate() + 30);
    this.maxDate = now;

    const tempDate = now.getDate();
    const tempHour = now.getHours();
    const tempMinute = now.getMinutes();
    const lastDay = new Date(now.getFullYear(), now.getMonth() + 1, 0);
    const latestDate = lastDay.getDate();

    this.hours.push({ key: '--', value: 'As soon as posible' });

    for (var i = tempHour; i < 24; i++) {
      var x = '';
      if (i < 10) x = '0' + i; else x = i + '';

      this.hours.push({ key: x, value: x });
    }
    this.minutes.push({ key: '--', value: '' });
    if (tempMinute > 50) {
      this.minutes.push({ key: '00', value: '00' });
    }
    for (var i = 10; i < 60; i += 10) {
      var x = '';
      if (i < 10) x = '0' + i; else x = i + '';
      if (i >= tempMinute) this.minutes.push({ key: x, value: x });
    }
  }

 
  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 700) ? 1 : 2;
  }

  ngOnInit() {
    this.isHandset = this.breakpointObserver.observe(Breakpoints.Handset);
    this.breakpoint = (window.innerWidth <= 700) ? 1 : 2;

    // this.homeServices.getCheckoutPageData().subscribe((response) => {
    //   this.checkoutPageData = response.Data;
    //   this.changeDetectorRef.markForCheck();
    // });
  }

  isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  onAddressPicked(addressPicked) {
    this.addressPicked = addressPicked;
    console.log(addressPicked);
  }

  onCheckShippingCost(addressPicked) {
    this.addressPicked = addressPicked;
    console.log(addressPicked);
  }

  doCheckoutAndGoPayment() {
    let vlidMsg = [];
    if (this.paymentMethod == '') vlidMsg.push('Must selected payment method');
    if (this.shippingMethod == '') vlidMsg.push('Must selected shipping method');
    if (this.fullName == '') vlidMsg.push('Must input fullname');
    if (this.phone == '') vlidMsg.push('Must input phone');
    if (this.email == '') vlidMsg.push('Must input email');
    if (this.getSelectedDate() == null) vlidMsg.push('Must choose receiving date');
    if (this.addressPicked == null || this.addressPicked.Address == '') vlidMsg.push('Must input address');
    if (vlidMsg.length > 0) {
      let dialogRef = this.dialog.open(AlertDialogComponent, {
          width: '400',
          data: { Title: 'Check your input(s)', Message: vlidMsg.join("<br>") }
        });
      return;
    }

  }
  doCheckVoucher() {
    //voucher
  }

  getSelectedDate() {
    const now = new Date();
    if (this.selectedDate == null) return null;
    if (now > this.selectedDate) {
      return null;
    }
    const tempDate = this.selectedDate.getDate();
    const tempHour = this.selectedHour=='--'?'00': this.selectedHour;
    const tempMinute = this.selectedMinute == '--' ? '00' : this.selectedMinute;

    var date = new Date(this.selectedDate.getFullYear(), this.selectedDate.getMonth() + 1, this.selectedDate.getDate(),
      parseInt(tempHour), parseInt(tempMinute));

    return date;
  }
}
