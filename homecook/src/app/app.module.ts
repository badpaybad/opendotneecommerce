import { NgModule, Component, Pipe, Directive, ChangeDetectorRef  } from '@angular/core';

import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClient, HttpClientModule, HttpHeaders, HttpParams } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';

import { AppConfigService } from './app-config.service';

import { AppComponent } from './app.component';
import { MenuTopComponent } from './menu-top/menu-top.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomePageComponent } from './home-page/home-page.component';
import { MealsMenuComponent } from './meals-menu/meals-menu.component';
import { AboutUsComponent } from './about-us/about-us.component';
//import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MyDashboardComponent } from './my-dashboard/my-dashboard.component';
import { MyTableComponent } from './my-table/my-table.component';
import { ProductDetailBottomSheetComponent } from './product-detail-bottom-sheet/product-detail-bottom-sheet.component';
import { YesNoDialogComponent } from './yes-no-dialog/yes-no-dialog.component';
import { MenuDetailBottomSheetComponent } from './menu-detail-bottom-sheet/menu-detail-bottom-sheet.component';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { ImageSliderDialogComponent } from './image-slider-dialog/image-slider-dialog.component';
import { ShoppingcartCheckoutComponent } from './shoppingcart-checkout/shoppingcart-checkout.component';
import { GoogleMapPickAddressComponent } from './google-map-pick-address/google-map-pick-address.component';
import { ShoppingcartListItemComponent } from './shoppingcart-list-item/shoppingcart-list-item.component';
import { EditorModule } from '@tinymce/tinymce-angular';

@NgModule({
  declarations: [
    AppComponent,
    MenuTopComponent,
    HomePageComponent,
    MealsMenuComponent,
    AboutUsComponent,
    MyNavComponent,
    MyDashboardComponent,
    MyTableComponent,
    ProductDetailBottomSheetComponent,
    YesNoDialogComponent,
    MenuDetailBottomSheetComponent,
    AlertDialogComponent,
    ImageSliderDialogComponent,
    ShoppingcartCheckoutComponent,
    GoogleMapPickAddressComponent,
    ShoppingcartListItemComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule, MatButtonModule, MatCheckboxModule, MatMenuModule, MatIconModule, MatGridListModule
    //, NgbModule.forRoot()
    , LayoutModule, MatToolbarModule, MatSidenavModule, MatListModule, MatCardModule, MatTableModule, MatPaginatorModule, MatSortModule
    , MatBottomSheetModule, MatSnackBarModule, MatDialogModule, MatExpansionModule, MatDividerModule, MatInputModule,
    MatRadioModule, MatDatepickerModule,MatNativeDateModule,MatSelectModule
    , EditorModule
    , AppRoutingModule
  ],
  entryComponents: [ProductDetailBottomSheetComponent, YesNoDialogComponent, MenuDetailBottomSheetComponent, AlertDialogComponent
    , ImageSliderDialogComponent],
  providers: [Title, AppConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
