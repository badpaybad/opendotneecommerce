import { Injectable, EventEmitter, Component, Pipe, Directive } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

import { NotificationMessage } from './view-model/NotificationMessage';
import { AppConfigService } from './app-config.service';

//import '../assets/jquery-1.12.4.min.js';
declare const $: any; // jQuery
import '../assets/jquery.signalR-2.2.3.min.js';
import '../assets/hubs.js';

@Injectable({
  providedIn: 'root'
})
export class SystemNotificationService {
  systemNotificationHub: any = null;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(private appConfig: AppConfigService
    , public snackBar: MatSnackBar) {
     

    // this.horizontalPosition = 'center';
    // this.verticalPosition = 'bottom';

   
    // $.connection.hub.url = appConfig.signalrUrl + '/signalr';

    // this.systemNotificationHub = $.connection.systemNotificationHub;
    // var _this = this;
    // this.systemNotificationHub.client.boardCastMessage = function (msg: NotificationMessage) {
    //   var data = JSON.parse(msg.DataJson);
   
    //    _this.snackBar.open(data.message, 'Dismiss', {
    //     duration: 5000, verticalPosition: _this.verticalPosition
    //     , horizontalPosition: _this.horizontalPosition
    //   });
    // };

    // $.connection.hub.start().done(function () {
    //   $.connection.systemNotificationHub.server.clientConnected();
    // });

    // //this.snackBarRef.onAction.subscribe(() => {
    // //  console.log(this.snackBarRef.dismissWithAction);
    // //});

  }
}



//https://github.com/aspnet/SignalR
//https://damienbod.com/2017/09/12/getting-started-with-signalr-using-asp-net-core-and-angular/
//import { signalR,HubConnection, HubConnectionBuilder } from '@aspnet/signalr';

//import * as signalR from '../assets/signalr.js';
//import  '../assets/signalr-protocol-msgpack.js'; 

//@Injectable({
//  providedIn: 'root'
//})
//export class SystemNotificationService {
//  private hubConnection: any;

//  constructor( private appConfig:AppConfigService) {
   
//    var hubUrl = this.appConfig.apiUrl + '/systemNotificationHub';

//    this.hubConnection = new signalR.HubConnection(hubUrl, { logger: signalR.LogLevel.Information });
    
//    this.hubConnection.on('boardCastMessage', (data: any) => {
//      console.log(data);
//    });

//    this.startSignalR();
//  }

//  public startSignalR() {
//    this.hubConnection.start()
//      //.then(() => {
//      //  this.connectionIsEstablished = true;
//      //  console.log('Hub connection started');
//      //  this.connectionEstablished.emit(true);
//      //})
//      .catch(err => {
//        console.log('Error while establishing connection, retrying...');
//        setTimeout(this.startSignalR(), 5000);
//      });
//  }
//}
