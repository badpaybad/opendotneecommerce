import { Injectable, isDevMode } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  apiUrl: string = 'http://localhost:6868/api';
  signalrUrl: string = 'http://localhost:6868';
  shoppingCartUrl: string = 'http://localhost:6868';

  mealsId:string;
  drinksId:string;
  menusId:string;
  imageUrl:string;
  baseHref:string;
  
  constructor(private meta: Meta) {
    // if (!isDevMode) {
    //    this.apiUrl = 'http://badpaybad.info/api';
    //   this.signalrUrl = 'http://badpaybad.info';
    //   this.shoppingCartUrl = 'http://badpaybad.info';    
    // }
    this.baseHref= this.getBaseHref();
    this.apiUrl= this.meta.getTag('name="apiUrl"').content;  
    this.signalrUrl= this.meta.getTag('name="signalrUrl"').content;
    this.shoppingCartUrl= this.meta.getTag('name="shoppingCartUrl"').content;
    this.imageUrl= this.meta.getTag('name="imageUrl"').content+ this.baseHref;
    
    this.mealsId= this.meta.getTag('name="mealsId"').content;  
    this.drinksId= this.meta.getTag('name="drinksId"').content;  
    this.menusId= this.meta.getTag('name="menusId"').content;  
  }

   getBaseHref() {
    return document.getElementsByTagName('base')[0].href;
  }
}
