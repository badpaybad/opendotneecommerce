import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuDetailBottomSheetComponent } from './menu-detail-bottom-sheet.component';

describe('MenuDetailBottomSheetComponent', () => {
  let component: MenuDetailBottomSheetComponent;
  let fixture: ComponentFixture<MenuDetailBottomSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuDetailBottomSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuDetailBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
