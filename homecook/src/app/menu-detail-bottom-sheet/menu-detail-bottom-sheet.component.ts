import { Component, OnInit, EventEmitter, Input, Inject, AfterContentInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { ImageSliderDialogComponent } from '../image-slider-dialog/image-slider-dialog.component';

import { ComboHomePage, DrinkHomePage, MenuHomePage, ProductDetailHomePage } from '../view-model/ComboHomePage';
import { HomePageService } from '../home-page.service';

@Component({
  selector: 'app-menu-detail-bottom-sheet',
  templateUrl: './menu-detail-bottom-sheet.component.html',
  styleUrls: ['./menu-detail-bottom-sheet.component.css']
})
export class MenuDetailBottomSheetComponent implements OnInit {
  products: Array<ProductDetailHomePage> = [];
  productColumns = ['ImageUrl', 'ProductCode', 'Gram', 'Price', 'Id'];
  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
    private homePageService: HomePageService,
    private bottomSheetRef: MatBottomSheetRef<MenuDetailBottomSheetComponent>,
    public dialog: MatDialog, private changeDetectorRef: ChangeDetectorRef) {
  }

  categoryTitle = '';

  ngOnInit() {
    this.categoryTitle = this.data.categoryTitle;

    this.homePageService.getProductsInMenu(this.data.categoryId).subscribe((products) => {
      this.products = products;
      this.changeDetectorRef.markForCheck();
    });
  }

  showImageSlider(productId: string) {
    var temp = this.products.filter(p => p.Id == productId);
    if (temp.length > 0) {
      var urlImgs: string[] = [];
      urlImgs.push(temp[0].ImageUrl);
      var d = this.dialog.open(ImageSliderDialogComponent, { width: '100%', data: { Title: temp[0].Title, urlImages: urlImgs } });
    }
  }

  addToCart(productId: string, quantity: number) {
    if (quantity <= 0) {
      let dialogRef = this.dialog.open(AlertDialogComponent, {
        width: '250px',
        data: { Title: 'Quantity must be at least 1', Message: 'Quantity must be at least 1' }
      });
      return;
    }
    this.homePageService.addToCart(productId, quantity);
    this.bottomSheetRef.dismiss();
  }

  close() {
    this.bottomSheetRef.dismiss();
  }
}
